import java.io.*;

/** Counts the number of bytes in a given directory
 */
public class DirectorySize{
	public static void main(String [] args){
		if(args.length != 1){// checks that directory name has been passed as argument
			System.err.println("Usage: DirectorySize <path to directory>");
			System.exit(1);
		}

		File fp = new File(args[0]);
		if(!fp.exists()){// checks if file exists
			System.err.println(args[0] + " does not exists.");
			System.exit(2);
		}
		if(!fp.isDirectory()){// checks if file is a directory
			System.err.println(args[0] + " is not a directory.");
			System.exit(3);
		}
		System.out.println(args[0] + " has " + getSize(fp) + " bytes");
	}

	/** Returns length of files in the subdirectories
	 *  + length of files in the root directory
	 * */
	public static long getSize(File dir){
		if (dir.isFile())
			return dir.length();

		File[] files = dir.listFiles();
		long size = 0;
		for(int i = 0; i < files.length; i++)
			size += getSize(files[i]);
		return size;
	}
}