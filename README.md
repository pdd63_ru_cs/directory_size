# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Traverse a given directory compute the length of all its subdirectories and files. Display the size of the directory.



### How do I get set up? ###
This program requires:
- passing the directory name as argument on command line `java DirectorySize <name of directory>`
- directory has to exists.
- file object with directory name has to return true to `obj.isDirectory()`

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact